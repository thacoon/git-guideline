Git Guidelines
==============

## Workflow

0. Every team member has to clone the project.
1. Check out a feature branch.
2. Do work in your feature branch, committing early and often.
    1. Apply updates from master
3. Create a pull request on GitLab.
4. Another team member must check the pull request and allow or deny merge.
    1. **Never merge your own pull request until someone in your team approves it!**
    2. Inform everyone about pull request.
5. Back to 1.

### Merge Request
Do not merge your commits directly into master, use merge requests (GitLab) or pull requests (GitHub).

Using GitLab I prefer to squash the commits using the *squash commits when merge request is accepted* option. If doing this you can skip *REBASE ALL YOUR CHANGES* and only use rebase during pull. Using the option


### When to commit

1. Often but never half done work.
    1. This does not mean you have to complete the whole, large feature but the commits are logical chunks.
    2. Do not commit different features in one.
2. Test before you commit.
    1. Write tests and run them before commit.
3. Write good commit messages, see "How to write a Git commit message".
4. Make use of branches
    1. Fixes are done in branches.
    2. Developing new features is done in branches.
    3. The main branch should be stable and should not be directly committed to.


## How to write a Git commit message

1. Separate subject from body with a blank line
2. Limit the subject line to 70 characters
    * `git config --global core.editor "nano -r 70"`
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 70 characters
7. Use the body to explain what and why vs. how


## How to work together

1. If you are not one of the maintainers (not responsible for it) create pull requests to commit to a repository.
2. Never directly commit to the master branch, use branches.

## How to write an issue

1. Keep the title short and descriptive.
2. Describe the issue as best as possible and give as much information as needed so the issue can be reproduced.
    1. System information
    2. Logs
    3. ...
3. Stay tuned for further questions.

## Branches
1. Branch names should contain the issue number and a short description, like `3275-add_server_logs` or for bug fixes `bug-3845-empty-comments-allowed`.
    1. Contain the number of the issue is one exists.
    2. A short hint what it is for.

## Gitignore
If you do not want to upload specific files you can create a *.gitignore* file in the base dir and add files or directories into it that should not be included, wildcards are allowed. This is called a blacklist.

```
data/*.json
__pycache__
```

Alternative you can use a whitelist. This means you specifically select which file types should be allowed.

```
# Ignore everything
*

# Allow specific file types with !
!.py
```

### Cheatsheet
![Git cyclus](guideline.png)

## References

#### Git: Dezentrale Versionsverwaltung im Team Grundlagen und Workflow
A book from René Preißel, Bjorn Stachmann

#### A Git Workflow for Agile Teams
http://reinh.com/blog/2009/03/02/a-git-workflow-for-agile-teams.html

#### Commit Often, Perfect Later, Publish Once: Git Best Practices
https://sethrobertson.github.io/GitBestPractices/

#### Merging vs Rebasing
https://www.atlassian.com/git/tutorials/merging-vs-rebasing#the-golden-rule-of-rebasing

#### Git submodules
https://git-scm.com/book/en/v2/Git-Tools-Submodules

#### Using Git As A Group
https://gist.github.com/zacanger/9119a32e3cd02e5ccc154526715a723c

#### Using Git in a team
http://jameschambers.co/writing/git-team-workflow-cheatsheet/

#### Getting Started with Git in a Team Environment
https://www.sitepoint.com/getting-started-git-team-environment/

#### Git squash commits
http://gitready.com/advanced/2009/02/10/squashing-commits-with-rebase.html

#### Dont' be scared of git rebase
https://nathanleclaire.com/blog/2014/09/14/dont-be-scared-of-git-rebase/

### –force considered harmful; understanding git’s –force-with-lease
http://blog.developer.atlassian.com/force-with-lease/
